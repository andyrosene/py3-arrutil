# ARR - Utility Functions

> **WARNING**
>
> This will likely be broken apart again once I find an implementation that I find reasonable. Until then, this is going
> to be a nice replacement for the existing, `common` package whose value has already been lost. I need to remember to
> continually preface these correctly or things will not work as I want.

## Prerequisites

* `arrelevator`
* `arrfs`
