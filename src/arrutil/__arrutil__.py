# fixme - a number of these functions need to be removed because they are found in another project all together.
from arrelevator import *
from arrfs import *
from getpass import getpass
from os import curdir, system
from subprocess import Popen, PIPE

__all__ = [
    'request_username',
    'request_password',
    'is_url_valid',
    'download_file',
    'staged_download_file',
    'staged_download_file_to_destination',
    'fetch_url_source',
    'is_mount_point_active',
    'unmount_by_mount_point',
    'is_share_dns_or_ip_available',
]


def request_username(modifier: str = None) -> str:
    """Request the username adding the given modifier to clarify purpose if provided.

    Args:
        modifier: Identifies the type of username needed if provided.

    Returns:
        str: The user provided username.
    """
    request_str = '{}Username:          '.format('' if modifier is None else '{} '.format(modifier))
    re_request_str = 'Re-enter {}Username: '.format('' if modifier is None else '{} '.format(modifier))

    while True:
        username = input(request_str)
        verified_username = input(re_request_str)

        if username == verified_username:
            return username

        print('[error] usernames did not match, try again.')


def request_password(modifier: str = None) -> str:
    """Request the password adding the given modifier to clarify purpose if provided.

    Args:
        modifier: Identifies the type of password needed if provided.

    Returns:
        str: The user provided password.
    """
    request_str = '{}Password:          '.format('' if modifier is None else '{} '.format(modifier))
    re_request_str = 'Re-enter {}Password: '.format('' if modifier is None else '{} '.format(modifier))

    while True:
        password = getpass(prompt=request_str)
        verified_password = getpass(prompt=re_request_str)

        if password == verified_password:
            return password

        print('[error] passwords did not match, try again.')


def is_url_valid(url: str) -> bool:
    """Return whether the given URL is valid or not. This also works for FTP servers.

    fixme(arr) - use either curl or wget depending on which exists. If neither exists, download curl and use that.

    Args:
        url: The URL to check.

    Returns:
        bool: Whether the given URL exists or not.
    """
    # # Uncomment to do using wget
    # return _system('wget -q --method=HEAD "{}"'.format(url)) == 0
    # Done using curl.
    return system('curl --head --silent --fail "{}" >/dev/null 2>/dev/null'.format(url)) == 0


def download_file(download_url: str, out_file: AFile = None, silent_download: bool = False):
    # fixme(arr) - this needs to be configurable. some cases I know this will be valid.
    if not is_url_valid(download_url):
        raise AssertionError('Cannot download invalid given URL {}'.format(download_url))

    # If an output file is specified...
    if out_file is not None:
        # raise an assertionerror if the file currently exists
        if out_file.exists():
            raise AssertionError(out_file.path + ' already exists')

        # Create the parent directory
        out_file.parent.create()
        download_dir = out_file.parent
    else:
        download_dir = ADir(curdir)

    permitted = PCache.can_write(download_dir.path)
    if not permitted:
        elevate('Downloading ' + download_url + ' to ' + download_dir.path)

    # Download the file using curl
    # result = system('curl --silent "{}" > "{}"'.format(download_url, file_path))
    #
    # Download file using wget
    result = system('{}wget{}{} "{}"'.format(
        'sudo ' if not permitted else '',
        ' --quiet' if silent_download else '',
        ' -O "{}"'.format(out_file.path) if out_file is not None else '',
        download_url
    ))
    if result != 0:
        raise AssertionError('Failed to download {}from {}.\nResult: {}'.format(
            '{} '.format(out_file) if out_file is not None else '', download_url, result))


def staged_download_file_to_destination(download_url: str,
                                        destination: AFile,
                                        custom_staging: ADir = None,
                                        overwrite: bool = False,
                                        silent_download: bool = False,
                                        silent_delete: bool = False):
    """Download into the staging directory and then copy once the download is done. This prevents a corrupted file from
    being kept.

    Args:
        download_url: The URL the file is being downloaded from.

        destination: The destination for the downloaded file.

        custom_staging: The custom staging directory to use if specified.
            fixme - I should be auto-incrementing staging directories instead of this implementation.

        overwrite: Whether or not to overwrite an existing file.

        silent_download: Whether the download output should be suppressed.

        silent_delete: Whether the delete message should be suppressed or not.
    """
    # Leave if overwrite is not allowed and the destination file exists.
    if not overwrite and destination.exists():
        return

    # Delete the destination file if it exists
    destination.delete(silent=silent_delete)

    # Use the specified staging if one is provided, else use the default.
    custom_staging = custom_staging or d_staging

    try:
        # Create the staging directory
        custom_staging.create()

        # create the path for the file being downloaded.
        d_file = custom_staging.child_file(destination.name)

        # Download the file
        download_file(download_url, d_file, silent_download)

        # Move the downloaded file to the desired destination.
        d_file.move(destination)

    finally:
        # delete the staging directory
        custom_staging.delete(silent=True)


def staged_download_file(download_url: str, file_name: str, silent_download: bool = False) -> (AFile, ADir):
    """Download into the staging directory and then creates a reference to the downloaded file using the given file
    name. This file reference is then returned to the caller for consumption. If there is a file with the given name in
    the staging directory on call, that file is deleted automatically. If something goes wrong, the staging directory
    will be deleted and an AssertionError thrown; otherwise, the caller is responsible for deleting the staging
    directory when done.

    Warnings:
        1.  At this time, this is only capable of downloading Files. Directory support will be considered later if
            needed.
        2.  The given file_name will automatically override the name that was going to be automatically given to the file.
            This design principle was selected to simplify the overall burden of maintaining the changing file names.

    Args:
        download_url: The URL the file is being downloaded from.
        file_name: The name of file resulting from the download.
        silent_download: Whether the download output should be suppressed.

    Returns:
        (AFile, ADir): The object allowing interaction with the downloaded file and the staging directory to simplify
            cleanup for the caller on success.
    """
    # create the object that will be returned to the caller.
    result = d_staging.child_file(file_name)

    # delete the file if it exists.
    result.delete()

    # download the requested file
    try:
        d_staging.create()
        download_file(download_url, result, silent_download)

    except Exception as e:
        # delete the staging directory only on error. also, progress the caught exception.
        d_staging.delete()
        raise e

    # return the downloaded file on success
    return result, d_staging


def fetch_url_source(url: str) -> str:
    """fixme(arr) - add dependency support to ensure CURL is present.

    Args:
        url: The URL to get the source from.

    Returns:
        str: todo(arr) - something
    """
    if not is_url_valid(url):
        raise AssertionError('Cannot fetch source from given URL ' + url)

    p1 = Popen(['curl', '--silent', str(url)],
               stdout=PIPE,
               stderr=PIPE,
               universal_newlines=True)
    stdout, stderr = p1.communicate()
    if stderr.strip() != '':
        raise AssertionError('Failed to retrieve source from {}.\n{}'.format(url, stderr))

    return stdout.strip()


def is_mount_point_active(mount_point: ADir) -> bool:
    """Check whether the given mount point is active or not.

    Args:
        mount_point: The given mount point.

    Returns:
        bool: True if the mount point is active, else False.
    """
    return system('cat /proc/mounts | grep -w "{}" >/dev/null 2>/dev/null'.format(mount_point.path)) == 0


def is_share_dns_or_ip_available(share_dns_or_ip: str) -> bool:
    """Check if the given share_dns_or_ip is available.

    Args:
        share_dns_or_ip: The given DNS name or IP address.

    Returns:
        bool: True if the given share_dns_or_ip is available, else False.
    """
    return system('ping -c1 -w3 {} >/dev/null 2>/dev/null'.format(share_dns_or_ip)) == 0


def unmount_by_mount_point(mount_point: ADir) -> bool:
    """Unmount anything mounted at the given mount point.

    Args:
        mount_point: The given mount point.

    Returns:
        bool: True if unmounting was performed, else False.

    Raises:
        AssertionError: Something unexpected prevented the unmounting of the entity at the given mount point.
    """
    if not is_mount_point_active(mount_point):
        return False

    elevate('Unmounting ' + mount_point.path)
    # Sync before unmounting to prevent incomplete write issues.
    system('sync')
    if system('sudo umount -f "{}"'.format(mount_point.path)) != 0:
        raise AssertionError('Encountered an issue unmounting: ' + mount_point.path)

    return True
