class ItemList(object):
    """This is meant to allow elements to be initialized while also adding to a global list. This is a nice feature when
    you want to have a list of all constants.
    """

    def __init__(self):
        self.items = []

    def append(self, item):
        self.items.append(item)
        return item

    def get(self):
        return self.items
